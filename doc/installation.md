# Ubuntu
I found these steps from [DigitalOcean Tutorials](https://www.digitalocean.com/community/tutorials) and tested them myself.

## Docker
0. Verify whether your machine had Docker installed. If it was already installed, go to Step 5. 
    ```
    sudo docker --version
    ```

1. Update existing list of packages
    
    ```
    sudo apt update
    ```

2. Get access to the repository
    ```
    sudo apt install apt-transport-https ca-certificates curl software-properties-common
    curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -
    sudo add-apt-repository "deb [arch=amd64] https://download.docker.com/linux/ubuntu focal stable"
    apt-cache policy docker-ce
    ```

3. Install Docker
    ```
    sudo apt install docker-ce
    ```

4. Verify
    ```
    sudo systemctl status docker
    ```
    Expect to see output like below
    ```
    Output
    ● docker.service - Docker Application Container Engine
        Loaded: loaded (/lib/systemd/system/docker.service; enabled; vendor preset: enabled)
        Active: active (running) since Tue 2020-05-19 17:00:41 UTC; 17s ago
    TriggeredBy: ● docker.socket
        Docs: https://docs.docker.com
    Main PID: 24321 (dockerd)
        Tasks: 8
        Memory: 46.4M
        CGroup: /system.slice/docker.service
                └─24321 /usr/bin/dockerd -H fd:// --containerd=/run/containerd/containerd.sock
    ```

5. Setup to execute docker command without sudo
    ```
    sudo usermod -aG docker ${USER}
    su - ${USER}
    groups
    ```

6. Done!!!

## Docker-Compose
0. Verify whether your machine had docker-compose installed. If it was already installed, go to Step 4.
    ```
    sudo docker-compose --version
    ```

1. Installation
    ```
    sudo curl -L "https://github.com/docker/compose/releases/download/1.29.2/docker-compose-$(uname -s)-$(uname -m)" -o /usr/local/bin/docker-compose
    ```

2. Set permission
    ```
    sudo chmod +x /usr/local/bin/docker-compose
    ```

3. Verify
    ```
    docker-compose --version
    ```
    Expect to see output like below
    ```
    Output
    docker-compose version 1.29.2, build 5becea4c
    ```

4. Done!!!

# Windows
## This is the [link](https://docs.docker.com/desktop/install/windows-install/)