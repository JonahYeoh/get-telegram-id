# Ubuntu
1. Go to folder src
```
cd src
```

2. Fill in your telegram token at config/config.yml
```
cp config/config.example.yml config/config.yml
nano config/config.yml # TELEGRAM_TOKEN: "insert-your-token-here"
#Ctrl + X then Y then ENTER
```

3. Start the bot
```
docker-compose up -d
```

4. Stop the bot
```
docker-compose kill root_app
```

# Windows
1. At where you store this project, open the terminal (CMD)
2. Go to folder src/config
3. Make a copy of config.example.yml and name it as config.yml
4. Right click config.yml and open with Notepad or any text editor of your choice
5. Fill in your telegram token # TELEGRAM_TOKEN: "insert-your-token-here"

6. Start the bot
```
docker-compose up -d
```

7. Stop the bot
```
docker-compose kill root_app
```

8. Windows user can view and manage the running container at Docker Desktop