docker-compose up --build --no-start

# Variables
container_registry=jonahyeoh
repo_name=get-telegram-id
tag_name=latest

docker tag src_root_app:latest $container_registry/$repo_name:$tag_name
docker push $container_registry/$repo_name:$tag_name

docker-compose down
