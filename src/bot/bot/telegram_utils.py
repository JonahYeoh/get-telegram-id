# import logging
from telegram import (
    Update,
)
from telegram.ext import (
    CallbackContext,
)
from telegram.constants import ParseMode, MessageOriginType

import config

# logger = logging.getLogger(__name__)


async def message_handle(update: Update, context: CallbackContext):
    message = update.message
    # sender
    from_user = getattr(message, 'from_user', None)
    reply_text = "Your Telegram Info:\t" + str(from_user)

    # forward_from
    forward_origin = getattr(message, 'forward_origin', None)
    if forward_origin is not None:
        if forward_origin.type is MessageOriginType.HIDDEN_USER:
            reply_text += f"\n\nThe author of the forwarded message is {forward_origin.sender_user_name}. Detailed information not available due to author's privacy setting."
        else:
            reply_text += "\n\nThe author of the forwarded message is " + str(forward_origin["sender_user"])

    await update.message.reply_text(
        reply_text, parse_mode=ParseMode.HTML
    )


async def start_handle(update: Update, context: CallbackContext):
    await update.message.reply_text(config.greeting_1_msg, parse_mode=ParseMode.HTML)
    