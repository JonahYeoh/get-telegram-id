from telegram.ext import (
    ApplicationBuilder, CommandHandler, MessageHandler, AIORateLimiter, filters
)

# import logging
import config
import my_utils as mu
import telegram_utils as tu

# logging.basicConfig(
#     filename="/logs/bot.log",
#     format="%(asctime)s - %(name)s - %(levelname)s - %(message)s", level=logging.INFO
# )


def run_bot() -> None:

    application = (
        ApplicationBuilder()
        .token(config.token)
        .concurrent_updates(True)
        .rate_limiter(AIORateLimiter(max_retries=5))
        .build()
    )

    application.add_handler(CommandHandler("start", tu.start_handle))

    application.add_handler(MessageHandler(filters.TEXT, tu.message_handle))
    
    application.add_error_handler(mu.error_handle)
    # start the bot
    application.run_polling()


if __name__ == "__main__":
    run_bot()
