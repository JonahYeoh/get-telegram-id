# How to Find Your Telegram ID
Are you confused about the differences between Telegram ID, username, and user ID? Don't worry, I was too!

Have you ever found yourself searching endlessly and resorting to forwarding messages to RawDataBot? I know I have, countless times. It's always a mixed feeling when forwarding messages from unknown bots, especially when there are multiple bots with similar names.

But why is forwarding messages to an unknown bot a bad idea? Well, there's a possibility that the bot author might store your information. Now, I'm not implying that the author of RawDataBot has bad intentions. In fact, I use it quite often when I can't build my own bot. However, I want to raise awareness about the potential risks. Convenience can often be the perfect bait to lure unsuspecting users.

# My Goal
I aim to build a bot that fulfills its purpose without storing any extra user information. Feel free to use my bot, but I encourage you to build your own!

# Find This Bot
- URL: https://t.me/Get_Telegram_Account_Info_Bot
- Search with Telegram: @Get_Telegram_Account_Info_Bot (https://t.me/Get_Telegram_Account_Info_Bot)

# Get Started
1. Clone this project 
    ```
    git clone git@gitlab.com:JonahYeoh/get-telegram-id.git
    cd get-telegram-id
    ```
2. Get Telegram Token from BotFather
3. Install docker and docker-compose
4. Start the bot